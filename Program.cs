﻿using System;
using System.Threading;
using Microsoft.Data.SqlClient;

namespace SqlClientSample
{
    class Program
    {
        private const string CnnStr = "Server=tcp:dike4-ag.ad.data3s.com,1433;Integrated Security=true;Initial Catalog=DATA3S_CZ";
        
        static void Main(string[] args)
        {
            Console.WriteLine("Connecting to: " + CnnStr);
            while (true)
            {
                try
                {
                    using SqlConnection cn = new(CnnStr);
                    using SqlCommand cmd = cn.CreateCommand();
                    cmd.CommandText = "select 1 from T0001_BE";
                    cn.Open();
                    var res = cmd.ExecuteScalar();
                    Console.WriteLine("Result: " + res);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                Thread.Sleep(10000);
            }
        }
    }
}
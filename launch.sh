#!/bin/bash
user=`klist -k service.keytab | grep 1 | sed -n -e 's/^.* //p'`
echo "Logging in as $user"
kinit $user -t service.keytab
echo 'Kerberos logged in'
# while :
# do
  # sleep 1h
  # echo "$(date) Renewing Kerberos ticket"
  # kinit -R
# done &
while :
do
  sleep 5h
  echo "$(date) Issuing new Kerberos ticket"
  kinit $user -t service.keytab
done &

dotnet $1
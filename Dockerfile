﻿FROM mcr.microsoft.com/dotnet/aspnet:5.0.0-buster-slim AS base

#https://www.codeproject.com/Articles/1272546/Authenticate-Net-Core-client-of-SQL-Server-with-In
RUN sed -i 's/DEFAULT@SECLEVEL=2/DEFAULT@SECLEVEL=1/g' /etc/ssl/openssl.cnf && \
	sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /etc/ssl/openssl.cnf && \
	sed -i 's/DEFAULT@SECLEVEL=2/DEFAULT@SECLEVEL=1/g' /usr/lib/ssl/openssl.cnf && \
	sed -i 's/MinProtocol = TLSv1.2/MinProtocol = TLSv1/g' /usr/lib/ssl/openssl.cnf
RUN apt-get update && apt-get -y install krb5-config krb5-user && \
	apt-get autoremove -y && \
	rm -rf /var/lib/apt/lists/*
COPY launch.sh /app/
RUN chmod 777 /app/launch.sh \
	&& ln -s /app/launch.sh /
COPY krb5.conf /etc/
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0.100-buster-slim AS build
WORKDIR /src
COPY . .
RUN dotnet publish "SqlClientSample.csproj" -c Release -o /app

FROM base AS final
COPY --from=build /app .
ENTRYPOINT /launch.sh SqlClientSample.dll